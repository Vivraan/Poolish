using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using TMPro;
using UnityEngine;

namespace com.leaguex.Poolish
{
    public class PoolGame : MonoBehaviour, IPunObservable
    {
        private enum State { WaitingToStart, Cueing, InMotion, End }

        private static PoolGame instance;

        [SerializeField] private Vector3 cameraPosition;
        [SerializeField] private Vector3 cameraRotationEuler;
        [SerializeField] private PoolBall[] balls;
        [SerializeField] private CueController cueController;
        [SerializeField] private TMP_Text poolGameDetails;
        private State state;
        private List<PoolBall> ballsInPlay;
        private Dictionary<PlayerController, int> scoreboard;
        private List<PlayerController> pcsInPlay = new List<PlayerController>();
        private int turn = 0;
        private PoolBall cueBall;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            state = State.WaitingToStart;
            poolGameDetails.text = string.Empty;
            cueBall = cueController.GetComponent<PoolBall>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out PlayerController pc))
            {
                if (scoreboard == null)
                {
                    scoreboard = new Dictionary<PlayerController, int>();
                }

                if (scoreboard.Count < 2)
                {
                    scoreboard.Add(pc, 0);
                    pcsInPlay.Add(pc);
                    pc.JoinPoolGame(this);
                    InitGame();
                }
            }
        }

        private void Update()
        {
            if (state == State.WaitingToStart)
            {
                return;
            }

            // CheckConnectedPlayers();
            ExecCueing();
            ExecInMotion();
            CheckEndGame();

            void ExecCueing()
            {
                if (state == State.Cueing)
                {
                    cueController.ShowCueStick();
                    cueController.CanShoot = true;
                }
            }

            void ExecInMotion()
            {
                if (state == State.InMotion)
                {
                    cueController.CanShoot = false;

                    if (ArePoolBallsStationary())
                    {
                        state = State.Cueing;
                        turn++;
                    }
                }
            }

            bool ArePoolBallsStationary()
            {
                if (!cueBall.IsAlmostStationary)
                {
                    return false;
                }

                if (ballsInPlay != null)
                {
                    foreach (var ball in ballsInPlay)
                    {
                        if (!ball.IsAlmostStationary)
                        {
                            return false;
                        }
                    }
                }

                return true;
            }

            void CheckEndGame()
            {
                if (ballsInPlay != null && ballsInPlay.Count == 0)
                {
                    state = State.End;

                    if (scoreboard[pcsInPlay[0]] > scoreboard[pcsInPlay[1]])
                    {
                        pcsInPlay[0].Won();
                        pcsInPlay[1].Lost();
                    }
                    else if (scoreboard[pcsInPlay[0]] < scoreboard[pcsInPlay[1]])
                    {
                        pcsInPlay[0].Lost();
                        pcsInPlay[1].Won();
                    }
                    else // draw is a loss for both
                    {
                        pcsInPlay[0].Lost();
                        pcsInPlay[1].Lost();
                    }
                }
            }
        }

        public void InitGame()
        {
            if (scoreboard.Count == 2)
            {
                ballsInPlay = new List<PoolBall>(balls);
                state = State.Cueing;
                UpdateDetails();
            }
        }

        public void StopCueing()
        {
            state = State.InMotion;
        }

        public bool IsTheirTurn(in PlayerController pc)
        {
            return pcsInPlay.Count == 2 && pcsInPlay[turn % 2] == pc;
        }

        public void PossessCamera(in Camera camera)
        {
            camera.transform.position = cameraPosition;
            camera.transform.rotation = Quaternion.Euler(cameraRotationEuler);
        }

        public void UpdateCueForceFactor(in float forceFactor)
        {
            cueController.ForceFactor = forceFactor;
        }

        public void UpdateCueAngle(in float angleFactor)
        {
            float angle = (angleFactor * 2f - 1f) * 180f;
            cueController.CueStickArm.rotation = Quaternion.Euler(0f, angle, 0f);
        }

        public void GrantPoint(PoolBall ball)
        {
            scoreboard[pcsInPlay[turn % 2]]++;
            UpdateDetails();
            RemoveFromPlay(ball);
        }

        public void RemoveFromPlay(PoolBall ball)
        {
            ballsInPlay?.Remove(ball);
        }

        private void UpdateDetails()
        {
            poolGameDetails.text = $"Turn: {turn + 1} | {pcsInPlay[0].NickName}: {scoreboard[pcsInPlay[0]]} | {pcsInPlay[1].NickName}: {scoreboard[pcsInPlay[1]]}";
        }

        void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                if (pcsInPlay.Count == 2)
                {
                    stream.SendNext(scoreboard[pcsInPlay[0]]);
                    stream.SendNext(scoreboard[pcsInPlay[1]]);
                }
                stream.SendNext(turn);
            }
            else
            {
                if (pcsInPlay.Count == 2)
                {
                    scoreboard[pcsInPlay[0]] = (int)stream.ReceiveNext();
                    scoreboard[pcsInPlay[1]] = (int)stream.ReceiveNext();
                }
                turn = (int)stream.ReceiveNext();
            }
        }
    }
}
