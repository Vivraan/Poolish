using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace com.leaguex.Poolish
{
    public class PoolBall : MonoBehaviour
    {
        [System.Serializable]
        public class UnityEventPoolBall : UnityEvent<PoolBall> { }

        [SerializeField] private bool isCueBall = false;
        [SerializeField] private UnityEventPoolBall onRemovedFromPlay;
        [SerializeField] private UnityEventPoolBall onGrantPoint;
        public bool IsCueBall => isCueBall;
        private Vector3 originalPosition;
        private Rigidbody rb;

        public bool IsAlmostStationary =>
            Mathf.Approximately(rb.velocity.sqrMagnitude, 0f)
            && Mathf.Approximately(rb.angularVelocity.sqrMagnitude, 0f);

        private void Awake()
        {
            rb = GetComponent<Rigidbody>();
        }

        private void Start()
        {
            originalPosition = transform.position;
        }

        private void Update()
        {
            int layersToIgnore = ~LayerMask.NameToLayer("PoolTable");
            if (!Physics.Raycast(transform.position, Vector3.down, 5f, layersToIgnore))
            {
                if (isCueBall)
                {
                    ResetPosition();
                    rb.velocity = Vector3.zero;
                    rb.angularVelocity = Vector3.zero;
                }
                else
                {
                    onRemovedFromPlay.Invoke(this);
                }
            }
        }

        public void ResetPosition()
        {
            transform.position = originalPosition;
        }

        public void GrantPoint()
        {
            onGrantPoint.Invoke(this);
        }

    }
}
