using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.leaguex.Poolish
{
    public class MirrorTransform : MonoBehaviour
    {
        [SerializeField] private Transform target;

        private void Update()
        {
            transform.position = target.transform.position;
            transform.rotation = target.transform.rotation;
        }
    }
}
