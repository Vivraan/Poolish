using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CueController : MonoBehaviour
{
    [SerializeField] private Transform cueStickArm;
    [SerializeField] private Transform cueStickTip;
    [SerializeField] private Vector3 maxForce;
    public Transform CueStickArm => cueStickArm;
    public float ForceFactor { private get; set; }
    private float cooldownDuration = 1f;
    private float cooldown;
    public bool CanShoot { private get; set; }
    private SphereCollider sphereCollider;
    private Rigidbody rb;

    private void Awake()
    {
        sphereCollider = GetComponent<SphereCollider>();
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (cooldown < cooldownDuration)
        {
            CanShoot = false;
            cooldown += Time.deltaTime;
        }
        else
        {
            CanShoot = true;
        }
    }

    public void Cue()
    {
        if (CanShoot)
        {
            Physics.Raycast(
                cueStickTip.transform.position,
                cueStickTip.transform.forward,
                out RaycastHit hit,
                1f,
                ~LayerMask.NameToLayer("PoolBall"));
            rb.AddForceAtPosition(maxForce * ForceFactor, hit.point, ForceMode.Impulse);
            CanShoot = false;
            cooldown = 0;

            cueStickArm.gameObject.SetActive(false);
        }
    }

    public void ShowCueStick()
    {
        cueStickArm.gameObject.SetActive(true);
    }
}
