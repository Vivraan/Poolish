using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.leaguex.Poolish
{
    public class Pocket : MonoBehaviour
    {
        private void OnCollisionEnter(Collision other)
        {
            if (other.transform.TryGetComponent(out PoolBall ball))
            {
                if (ball.IsCueBall)
                {
                    ball.ResetPosition();
                }
                else
                {
                    ball.GrantPoint();
                    Destroy(ball.gameObject);
                }
            }
        }
    }
}