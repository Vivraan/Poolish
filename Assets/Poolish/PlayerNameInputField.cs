using Photon.Pun;
using TMPro;
using UnityEngine;

namespace Pun2Tutorial
{
    [RequireComponent(typeof(TMP_InputField))]
    public class PlayerNameInputField : MonoBehaviour
    {
        #region Private Constants

        const string PrefKeyPlayerName = "PlayerName";

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            var defaultName = string.Empty;
            var inputField = GetComponent<TMP_InputField>();

            if (inputField)
            {
                if (PlayerPrefs.HasKey(PrefKeyPlayerName))
                {
                    defaultName = PlayerPrefs.GetString(PrefKeyPlayerName);
                    inputField.text = defaultName;
                }
            }

            PhotonNetwork.NickName = defaultName;
        }

        #endregion

        #region Public Methods

        public void SetPlayerName(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                Debug.LogError("PlayerName is null or empty.");
                return;
            }

            PhotonNetwork.NickName = value;
            PlayerPrefs.SetString(PrefKeyPlayerName, value);
        }

        #endregion
    }
}
