using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace com.leaguex.Poolish
{
    public class GameManager : MonoBehaviourPunCallbacks
    {
        [SerializeField] private PlayerController pcPrefab;
        [SerializeField] private Vector3 spawnPoint = new Vector3(0f, 5f, 0f);
        [SerializeField] private string loginScene = "Login";

        private void Start()
        {
            if (!PhotonNetwork.IsConnected)
            {
                SceneManager.LoadScene(loginScene);
                return;
            }

            if (!PlayerController.LocalPlayerInstance)
            {
                Debug.Log($"Instantiating local player from {SceneManagerHelper.ActiveSceneName}");

                PhotonNetwork.Instantiate(pcPrefab.name, spawnPoint, Quaternion.identity);
            }
            else
            {
                Debug.Log($"Local player already exists in {SceneManagerHelper.ActiveSceneName}");
            }
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            base.OnPlayerEnteredRoom(newPlayer);
            MasterLoadLevel();
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            base.OnPlayerLeftRoom(otherPlayer);
            MasterLoadLevel();
        }

        public override void OnLeftRoom()
        {
            base.OnLeftRoom();
            SceneManager.LoadScene(loginScene);
        }

        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
        }

        private static void MasterLoadLevel()
        {
            if (PhotonNetwork.IsMasterClient)
            {
                PhotonNetwork.LoadLevel(SceneManagerHelper.ActiveSceneBuildIndex);
            }
        }
    }
}
