using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace com.leaguex.Poolish
{
    public class LoginManager : MonoBehaviour
    {
        public static LoginManager Instance { get; private set; }

        [SerializeField] private bool skipLogin = true;
        [SerializeField] private string loginUri = "http://13.250.35.182:5000/v1/user/login";
        [SerializeField] private string updateUri = "http://13.250.35.182:5000/v1/user/update-profile";
        [SerializeField] private TMP_InputField usernameInput;
        [SerializeField] private TMP_InputField passwordInput;
        [SerializeField] private TextMeshProUGUI feedbackText;
        [SerializeField] private string nextLevel = "AvatarSelection";
        private bool isGuest = true;
        private float poolGamesWon = 0;

        private void Awake()
        {
            if (!Instance)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }

            DontDestroyOnLoad(gameObject);
        }

        public void ValidateLogin()
        {
            if (skipLogin)
            {
                SceneManager.LoadScene(nextLevel);
                return;
            }

            if (string.IsNullOrWhiteSpace(usernameInput.text) || string.IsNullOrWhiteSpace(passwordInput.text))
            {
                feedbackText.text = "Username or password is empty.";
                return;
            }

            StartCoroutine(GetUserInfo(loginUri));

            IEnumerator GetUserInfo(string uri)
            {
                feedbackText.text = "Logging in...";
                var playerLogin = new PlayerLogin { email = usernameInput.text, password = passwordInput.text };
                using var request = UnityWebRequest.Post(uri, JsonUtility.ToJson(playerLogin));
                request.timeout = 20;

                yield return request.SendWebRequest();

                switch (request.result)
                {
                    case UnityWebRequest.Result.Success:
                        var data = JsonUtility.FromJson<RequestData>(request.downloadHandler.text);
                        poolGamesWon = data.pool_games_won;
                        isGuest = string.IsNullOrEmpty(data.userid);

                        SceneManager.LoadScene(nextLevel);
                        break;

                    case UnityWebRequest.Result.ConnectionError:
                        feedbackText.text = "Either your internet is off or the server didn't respond on time.";
                        break;

                    default:
                        feedbackText.text = $"Something went wrong. Contact the developer with this error - {request.responseCode}: {request.error}";
                        break;
                }
            }
        }

        public void SendVictoryDetails()
        {
            StartCoroutine(SendDetails(updateUri));

            IEnumerator SendDetails(string uri)
            {
                var data = new RequestData { pool_games_won = ++poolGamesWon };
                var formatter = new BinaryFormatter();
                using var ms = new MemoryStream();
                formatter.Serialize(ms, data);
                using var request = UnityWebRequest.Put(uri, ms.ToArray());

                yield return request.SendWebRequest();

                switch (request.result)
                {
                    case UnityWebRequest.Result.Success:
                        break;

                    case UnityWebRequest.Result.ConnectionError:
                        Debug.Log("Either your internet is off or the server didn't respond on time.");
                        break;

                    default:
                        Debug.Log($"Something went wrong. Contact the developer with this error - {request.responseCode}: {request.error}");
                        break;
                }
            }
        }
    }

    [System.Serializable]
    public struct PlayerLogin
    {
        public string email;
        public string password;
    }

    [System.Serializable]
    public struct RequestData
    {
        public string userid;
        public float pool_games_won;
    }
}
