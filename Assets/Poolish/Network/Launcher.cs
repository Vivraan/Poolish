using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace com.leaguex.Poolish
{
    public class Launcher : MonoBehaviourPunCallbacks
    {
        [SerializeField] private TextMeshProUGUI feedbackText;
        [SerializeField] private List<Button> visibleButtons;
        [SerializeField] private byte maxPlayers = 4;
        [SerializeField] private string levelToLoad = "GameRoom";
        public const string CpKeyAvatar = "av";
        public const string CpKeyAvatarColor = "avc";

        bool isConnecting;
        private string avatar;

        private void Awake()
        {
            PhotonNetwork.AutomaticallySyncScene = true;
        }

        private void Start()
        {
            foreach (var button in visibleButtons)
            {
                button.onClick.AddListener(() =>
                {
                    foreach (var otherButton in visibleButtons)
                    {
                        if (otherButton != button)
                        {
                            otherButton.interactable = false;
                        }
                    }

                    var tmpText = button.GetComponentInChildren<TMP_Text>();
                    avatar = tmpText.text;
                    var avatarColor = PunColor.FromColor(tmpText.color);

                    PhotonNetwork.LocalPlayer.SetCustomProperties(new ExitGames.Client.Photon.Hashtable
                    {
                        [CpKeyAvatar] = avatar,
                        [CpKeyAvatarColor] = avatarColor
                    });
                });
            }
        }

        public void Connect()
        {
            feedbackText.text = string.Empty;
            isConnecting = true;

            // controlPanel.gameObject.SetActive(false);

            if (PhotonNetwork.IsConnected)
            {
                LogFeedback("Joining Room...");
                PhotonNetwork.JoinRandomRoom();
            }
            else
            {
                LogFeedback("Connecting...");
                isConnecting = PhotonNetwork.ConnectUsingSettings();
            }
        }

        public override void OnConnectedToMaster()
        {
            base.OnConnectedToMaster();

            if (isConnecting)
            {
                LogFeedback("Master client connected. Joining Room...");
                PhotonNetwork.JoinRandomRoom();
                isConnecting = false;
            }
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            base.OnJoinRandomFailed(returnCode, message);

            LogFeedback("No rooms available, creating one...");
            PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = maxPlayers });
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            base.OnDisconnected(cause);

            if (cause == DisconnectCause.DnsExceptionOnConnect)
            {
                LogFeedback("DNS lookup failed, maybe check if you're connected to the internet?");
            }
            else
            {
                LogFeedback($"Disconnected. Cause: {cause}.");
            }

            isConnecting = false;

            foreach (var button in visibleButtons)
            {
                button.interactable = true;
            }
        }

        public override void OnJoinedRoom()
        {
            base.OnJoinedRoom();

            LogFeedback($"Joined room with {PhotonNetwork.CurrentRoom.PlayerCount} players!");
            LogFeedback($"Chosen avatar: {avatar}");

            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                PhotonNetwork.LoadLevel(levelToLoad);
            }
        }

        private void LogFeedback(in string message)
        {
            if (!feedbackText)
            {
                return;
            }

            Debug.Log(message);
            feedbackText.text += System.Environment.NewLine + message;
        }
    }

    public static class PunColor
    {
        public static object[] FromColor(in Color color)
        {
            return new object[] { color.r, color.g, color.b, color.a };
        }

        public static Color ToColor(in object[] colorList)
        {
            return new Color(
                (float)colorList[0], (float)colorList[1],
                (float)colorList[2], (float)colorList[3]);
        }
    }
}
