using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace com.leaguex.Poolish
{
    public class PlayerController : MonoBehaviourPun
    {
        private enum State { Idle, Playing, Winner, Loser };

        public static PlayerController LocalPlayerInstance { get; private set; }

        [SerializeField] private TMP_Text avatarText;
        [SerializeField] private TMP_Text nicknameText;
        [SerializeField] private MeshRenderer body;
        [SerializeField] private Vector3 cameraPosition;
        [SerializeField] private float moveSpeed = 5f;
        [SerializeField] private float cameraRotateSpeed = 1f;
        public string NickName => photonView.Owner.NickName;
        private State state;
        private PoolGame poolGame;
        private Joystick leftJoystick;
        private Joystick rightJoystick;
        private Slider cueForceSlider;
        private Slider cueAngleSlider;
        private Button cueButton;
        private Transform pcCameraArm;
        private Camera pcCamera;
        private TMP_Text playerDetails;

        private void Awake()
        {
            if (photonView.IsMine)
            {
                LocalPlayerInstance = this;
            }

            DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
            if (photonView.IsMine)
            {
                body.gameObject.SetActive(false);
            }

            var properties = photonView.Owner.CustomProperties;
            avatarText.text = (string)properties[Launcher.CpKeyAvatar];
            avatarText.color = PunColor.ToColor((object[])properties[Launcher.CpKeyAvatarColor]);
            nicknameText.text = photonView.Owner.NickName;

            state = State.Idle;
            LatchOnToScene();
        }

        private void LateUpdate()
        {
            // Always ensure temporary object references are valid. This *will* change when players join as the scene is reloaded.
            LatchOnToScene();

            if (photonView.IsMine)
            {
                if (state == State.Idle)
                {
                    cueForceSlider.gameObject.SetActive(false);
                    cueAngleSlider.gameObject.SetActive(false);
                    cueButton.gameObject.SetActive(false);
#if UNITY_EDITOR
                    leftJoystick.gameObject.SetActive(false);
                    rightJoystick.gameObject.SetActive(false);
                    Move(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
#elif UNITY_ANDROID || UNITY_IOS
                    leftJoystick.gameObject.SetActive(true);
                    rightJoystick.gameObject.SetActive(true);
                    Move(leftJoystick.Horizontal, leftJoystick.Vertical, rightJoystick.Horizontal, rightJoystick.Vertical);
#endif
                }
                else if (state == State.Playing)
                {
                    cueForceSlider.gameObject.SetActive(true);
                    cueAngleSlider.gameObject.SetActive(true);
                    cueButton.gameObject.SetActive(true);

                    leftJoystick.gameObject.SetActive(false);
                    rightJoystick.gameObject.SetActive(false);

                    Play();
                }
                else if (state == State.Winner)
                {
                    playerDetails.text += "Uploading games won to cloud.";
                    LoginManager.Instance.SendVictoryDetails();
                }
            }

            void Move(float horizontal, float vertical, float rotX, float rotY)
            {
                pcCameraArm.transform.position = transform.position;

                var dx = vertical * pcCamera.transform.forward;
                var dz = horizontal * pcCamera.transform.right;
                var displacement = moveSpeed * Time.deltaTime * (dx + dz).normalized;
                displacement.y = 0;
                transform.position += displacement;

                var dRotX = rotX * cameraRotateSpeed * Time.deltaTime;
                pcCameraArm.Rotate(Vector3.up, dRotX);
                body.transform.Rotate(Vector3.up, dRotX);
                var eulerAngles = pcCamera.transform.eulerAngles;
                eulerAngles.x += -rotY * cameraRotateSpeed * Time.deltaTime;
                pcCamera.transform.eulerAngles = eulerAngles;
            }

            void Play()
            {
                poolGame?.PossessCamera(pcCamera);

                if (poolGame.IsTheirTurn(this))
                {
                    poolGame.UpdateCueForceFactor(cueForceSlider.value);
                    poolGame.UpdateCueAngle(cueAngleSlider.value);
                }
            }
        }

        public void JoinPoolGame(in PoolGame poolGame)
        {
            this.poolGame = poolGame;
            state = State.Playing;
        }

        public void Won()
        {
            state = State.Winner;
        }

        public void Lost()
        {
            state = State.Loser;
        }

        /// <summary>
        /// Get/Instantiate temporary objects
        /// </summary>
        private void LatchOnToScene()
        {
            if (photonView.IsMine)
            {
                if (!playerDetails)
                {
                    playerDetails = GameObject.Find("LocalPlayerDetails").GetComponent<TMP_Text>();
                    playerDetails.text = nicknameText.text + System.Environment.NewLine + avatarText.text;
                }

                if (!pcCamera)
                {
                    pcCamera = Camera.main;
                    pcCameraArm = pcCamera.transform.parent;
                    pcCamera.transform.localPosition = cameraPosition;
                }

                if (!leftJoystick || !rightJoystick)
                {
                    leftJoystick = GameObject.FindGameObjectWithTag("LeftJoystick").GetComponent<Joystick>();
                    rightJoystick = GameObject.FindGameObjectWithTag("RightJoystick").GetComponent<Joystick>();
                }

                if (!cueForceSlider || !cueAngleSlider || !cueButton)
                {
                    cueForceSlider = GameObject.FindGameObjectWithTag("CueForceSlider").GetComponent<Slider>();
                    cueAngleSlider = GameObject.FindGameObjectWithTag("CueAngleSlider").GetComponent<Slider>();
                    cueButton = GameObject.FindGameObjectWithTag("CueButton").GetComponent<Button>();
                }
            }
        }
    }
}
