using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

namespace com.leaguex.Poolish
{
    public class QuitHandler : MonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape) && PhotonNetwork.IsConnected)
            {
                Application.Quit();
            }
        }
    }
}
